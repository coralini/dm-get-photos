class DmGetPhotos extends Polymer.Element {

  static get is() {
    return 'dm-get-photos';
  }

  static get properties() {
    return {
      host: String,
      params: {
        type: Object,
        value: () =>({
          "client_id":"784e7ff3afa997b49bce4f2c4b1df3131e452550f5a59a19ebe56b561ec5d9f5"
        })
      }
    };
  }
  _successFn(data){
    //debugger;
    const results = [];
    data.detail.results.forEach(result => {
      let item = {
        image: [
          {
            src:result.urls.regular,
            styling:"regular"
          }
        ],
        description:result.description,
        multipleicons:[
          {
            icons:[
              {
                "icon": "icons:visibility", 
                "value": result.width, 
                "color": "blue"
              }, 
              {
                "icon": "icons:favorite", 
                "value": result.likes, 
                "color":"red"
              }, 
              {
                "icon": "icons:cloud-download", 
                "value": result.height, 
                "color":"default"
              }
            ]
          }
        ]
      };
      results.push(item);
    });
    this.dispatchEvent(new CustomEvent('photo-results',{detail:results}));
  
  }

  starterMethod(input = ''){
    this.params.query = input;
    this.$.getPhotos.generateRequest();
  }
}

customElements.define(DmGetPhotos.is, DmGetPhotos);
